module.exports = {
    title: '老六🔥🌟👍-Blog',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/laoliublog/',
    head: [
        ['link', { rel: 'shortcut icon', type: "image/x-icon", href: `/favicon.ico` }]
    ],
    dest: 'public'
}